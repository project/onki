<?php

/**
 * @file
 * ONKI configuration UI.
 */

/**
 * Implements hook_admin_settings_form().
 */
function onki_admin_settings_form($form, &$form_state) {
  $form = array();

  $form['onki_global_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('ONKI Configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['onki_global_settings']['onki_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('ONKI / Finto domain'),
    '#description' => t('URL for connecting to ONKI Web Service API V2. See !link.', array('!link' => l(t('the API documentation'), 'https://code.google.com/p/onki-light/wiki/RESTv1'))),
    '#default_value' => variable_get('onki_domain', 'http://onki.fi/'),
    '#disabled' => FALSE,
    '#size' => 100,
    '#maxlength' => 512,
  );

  $form['onki_global_settings']['onki_api_version'] = array(
    '#type' => 'select',
    '#title' => t('Select used API version'),
    '#description' => t('The used ONKI API version. See !link. Finto uses v1.', array('!link' => l(t('the documentation for the APIs'), 'https://code.google.com/p/onki-light/wiki/RESTv1'))),
    '#default_value' => variable_get('onki_api_version', ONKI_DEFAULT_API_VERSION),
    '#options' => array(
      '1' => t('ONKI API v1'),
      '2' => t('ONKI API v2'),
    ),
  );

  $form['onki_global_settings']['onki_autocomplete_result_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Autocomplete result count'),
    '#description' => t('Define the maximum count of autocomplete results to be shown. Any number between 5 and 30 is a good choice.'),
    '#default_value' => variable_get('onki_autocomplete_result_count', ONKI_DEFAULT_RESULT_COUNT),
    '#size' => 2,
  );

  $form['onki_global_settings']['onki_autocomplete_show_frequency'] = array(
    '#type' => 'checkbox',
    '#title' => t('Autocomplete show frequency'),
    '#description' => t('If checked, shows how many times a term has been used before in the autocomplete list.'),
    '#default_value' => variable_get('onki_autocomplete_show_frequency', 0),
  );

  $form['onki_global_settings']['onki_autocomplete_filter_identical'] = array(
    '#type' => 'checkbox',
    '#title' => t('Autocomplete filter identical results'),
    '#description' => t('If checked, filters out completely identical terms (i.e same title and same concept hierarchy, but not necessarily same uri).'),
    '#default_value' => variable_get('onki_autocomplete_filter_identical', 0),
  );

  $form['onki_global_settings']['onki_result_language'] = array(
    '#type' => 'select',
    '#title' => t('Results language'),
    '#description' => t('Define the language for the results.'),
    '#default_value' => variable_get('onki_result_language', ONKI_DEFAULT_LANGUAGE),
    '#options' => array(
      'fi' => t('Finnish'),
      'sv' => t('Swedish'),
      'en' => t('English'),
    ),
  );

  $form['onki_global_settings']['onki_vocabulary'] = array(
    '#type' => 'textfield',
    '#title' => t('ONKI vocabulary name'),
    '#description' => t('Give ONKI vocabulary name. Ex. <em>yso, allars, koko, etc.</em>. <strong>Koko covers all ontologies</strong>.'),
    '#default_value' => variable_get('onki_vocabulary', ONKI_DEFAULT_VOCABULARY),
    '#size' => 5,
  );

  $form['onki_global_settings']['onki_method_name'] = array(
    '#type' => 'textfield',
    '#title' => t('ONKI method name'),
    '#description' => t('Give ONKI method name. See methods from the API !link. For most cases <strong>search</strong> is a valid choice here.', array('!link' => l(t('documentation'), 'http://onki.fi/api/'))),
    '#default_value' => variable_get('onki_method_name', 'search'),
    '#size' => 5,
  );

  $form['onki_global_settings']['onki_access_key'] = array(
    '#type' => 'textfield',
    '#title' => t('ONKI Access Key'),
    '#description' => t('Signup key for using ONKI Web Service. See !link. The key is dependent on your IP address.', array('!link' => l(t('the signup page'), 'http://onki.fi/en/signup/'))),
    '#default_value' => variable_get('onki_access_key', ''),
    '#size' => 40,
  );

  $form['onki_global_settings']['onki_query_cache_ttl'] = array(
    '#type' => 'select',
    '#title' => t('Cache lifetime'),
    '#description' => t('The time-to-live value for query responses from ONKI.'),
    '#default_value' => variable_get('onki_query_cache_ttl', '86400'),
    '#options' => array(
      0 => t('No cache'),
      500 => t('5 min'),
      3600 => t('1 hour'),
      86400 => t('24 hours'),
      604800 => t('7 days'),
    ),
  );

  $form['onki_global_settings']['onki_autocomp_min_strlen'] = array(
    '#type' => 'select',
    '#title' => t('Minimum string length'),
    '#description' => t('The minimum length for autocompletion queries.'),
    '#default_value' => variable_get('onki_autocomp_min_strlen', 2),
    '#options' => array(
      1 => 1,
      2 => 2,
      3 => 3,
      4 => 4,
      5 => 5,
      6 => 6,
      7 => 7,
    ),
  );

  return system_settings_form($form);
}
