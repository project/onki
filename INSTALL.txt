Installation
------------

1. Download or clone ONKI module from Sandbox:

git clone http://git.drupal.org/project/onki.git onki
http://drupal.org/project/onki

2. Enable ONKI module:

/admin/modules

3. Configure ONKI module's settings:

/admin/config/content/onki

The settings selection is quite broad but the example values provided work in
most cases.

4. Testing the service

After you've set up all the settings you can try the service out. If you
attached Onki to article content type you browse to:

/node/add/article

You should see an Onki field which allows you to add new terms. The autocomplete
feature should kick in when you start to type in your terms. If not check your
settings and specially your language and API key settings.

5. Managing display

After you save your article you see the Onki URL rather the human-readable
title. You can fix this by managing content type's display. So for example
if you are using the article content type you browse to:

admin/structure/types/manage/article/display

where you can select Onki field as the format. You should do this for both
default and teaser.

Thats it! Congratz!
