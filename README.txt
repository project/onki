#####################
# Onki service ######
#####################

UPDATE:
Onki is changing names to Finto (http://finto.fi) and will be moved to the
servers of the Finnish National Library. This most likely requires you to change
the address from http://onki.fi to http://finto.fi for the api-calls.

The api in use in the onki.module is a legacy api and certain parts will shortly
be updated to support the new api for performance reasons.


The ONKI service (http://onki.fi/) contains Finnish and international
ontologies, vocabularies and thesauri needed for publishing your content
cost-efficiently on the Semantic Web. Ontologies are conceptual models
identifying the concepts of a domain. They contain machine "understandable"
descriptions of the relations between the concepts.

ONKI is published and maintained by Semantic Computing Research Group SeCo.
It is part of the on-going project to build a national semantic web
infrastructure to Finland (FinnONTO).

#####################
# Onki module #######
#####################

Onki Drupal module integrates Onki service to Drupal so that you can
integrate Onki into Drupal taxonomies and therefore use Onki terms to
describe desired content types.

#####################
# Preparations ######
#####################

To set up ONKI module you have to have at least one content type (i.e. article)
and a taxonomy (i.e. tags) configured in your system.

To create a content type browse to:
/admin/structure/types/add

To create a taxonomy browse to:
/admin/structure/taxonomy/add

To use this module you need to register an API key for Onki service. The API
key is per IP address.

#####################
# Other information #
#####################

This module has been developed by Teemo Tebest (Yleisradio) and Tomi Mikola
(Wunderkraut). The module is currently used in production by Svenska Yle.

Project's sandbox page:
http://drupal.org/sandbox/teelmo/1596454

Git repository usage:
git clone teelmo@git.drupal.org:sandbox/teelmo/1596454.git onki

The above command should be run for example in
your {drupal_root}/sites/all folder.

Drupal Version:
Drupal 7.x, tested with 7.17
