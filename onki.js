(function ($) {
  Drupal.behaviors.onki = {
    attach: function(context) {
      $("input#onki_autocomplete").once(function() {
        Drupal.behaviors.onki.fixAutocompleteEvents();
        Drupal.behaviors.onki.refreshButtons();
        Drupal.behaviors.onki.rfbh();
        $("input#onki_autocomplete").parents("form").submit(function() {
          Drupal.behaviors.onki.moveValues();
        });
        $("input#onki_autocomplete").unbind('autocompleteSelect');
        $("input#onki_autocomplete").bind('autocompleteSelect', function(event) {
          // Get the URI value. At this point this
          // can be ONKI URI or new term.
          var selid = $(this).val();
          var selparts = selid.split('|');
          $("div#onki-tags-container").addOnkiTerm(selparts[1], selparts[2]);
          $(this).val("");
        });
      });
    },

    fixAutocompleteEvents: function() {
      if (Drupal.jsAC) {
        // This augments the hidePopup function in Drupal core to emit an event.
        // We do this in many modules, so we need to check if it's done already.
        if (typeof Drupal.jsAC.prototype.oldhp == "undefined") {
          Drupal.jsAC.prototype.oldhp = Drupal.jsAC.prototype.hidePopup;
          Drupal.jsAC.prototype.hidePopup = function (keycode) {
            var isselected = this.selected;
            Drupal.jsAC.prototype.oldhp.call(this, keycode);
            if (isselected) {
              $(this.input).trigger('autocompleteSelect');
            }
          };
        }
      }
    },

    // Function for refreshing button descriptions and concept trees.
    refreshButtons: function() {
      $("div.onkiterm").each(function() {
        $("div#onki-tags-container").addOnkiTerm($(this).attr("data-id"));
      });
    },

    // Function for refreshing the button handlers for term buttons.
    rfbh: function(obj) {
      if (typeof obj == "undefined") {
        obj = $("div.onkiremove");
      }
      obj.unbind("click");
      obj.click(function() {
        $(this).parent().remove();
      });
    },

    // Function that moves the values from the freebase field to the hidden real
    // taxonomy field.
    moveValues: function() {
      var onki_idlist = Drupal.behaviors.onki.getListOfOnkiValues();
      $("input.hidden_onki_field").val(onki_idlist.join(","));
    },
    getListOfOnkiValues: function() {
      var onki_idlist = []; // initialise array
      $("div#onki-tags-container div.onkiterm").each(function() {
          onki_idlist.push($(this).attr("data-id"));
      });
      return onki_idlist;
    },
  };

  /**
  * Prototype to look up concept tree / broader transitive for a Finto term.
  */
  $.fn.addOnkiTerm = function(onkiId, onkiLabel) {
    if (typeof onkiId == "undefined") return false;
    var url = Drupal.settings.basePath + 'onki/json/concepttree';
    var element = this;
    var termObj = $("div.onkiterm[data-id='"+ onkiId +"']");

    // To make the interface faster, show the label immediately!
    if (termObj.length == 0 && onkiLabel != "") {
      element.append('<div class="onkiterm" data-id="' + onkiId + '"><span class="description">' + onkiLabel + '</span><div class="onkiremove">&otimes;</div></div>');
      termObj = $("div.onkiterm[data-id='"+ onkiId +"']");
      Drupal.behaviors.onki.rfbh(termObj.find(".onkiremove"));
    }

    // Fetch the concept tree / broader transitive from the server.
    $.post(url , {uri: onkiId}, function(data) {
      var concepttree = [];
      $.each(data['parents'], function(key, obj) {
        if (obj.description) concepttree.push(obj.description);
      });
      var onkidescription = (data['vocabulary'] ? data['vocabulary']+ ': ' : '') + (data['description'] ? data['description'] : data['uri']);
      // Check if we are adding a term that already exists.
      if (termObj.length == 0) {
        element.append('<div class="onkiterm" data-id="' + data['uri'] + '"><span class="description">' + onkidescription + '</span><div class="onkiremove">&otimes;</div>' + (concepttree.length > 0 ? '<div class="concepttree">' + concepttree.join(" &raquo; ") + '</div>' : '') + "</div>");
        termObj = $("div.onkiterm[data-id='"+ onkiId +"']");
      }
      else {
        termObj.find("div.description").html(onkidescription);
        if (concepttree.length > 0) {
          var concepttreecontainer = termObj.find("div.concepttree");
          if (concepttreecontainer.length > 0) concepttreecontainer.html(concepttree.join(" &raquo; "));
          else termObj.append('<div class="concepttree">' + concepttree.join(" &raquo; ") + '</div>');
        }
      }
      Drupal.behaviors.onki.rfbh(termObj.find(".onkiremove"));
      Drupal.behaviors.onki.moveValues();
    });
  }
})(jQuery);
